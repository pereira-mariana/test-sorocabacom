## Test Sorocabacom

Code developed for the Frontend Developer test from Sorocabacom.

## Building

You'll need [Node.js](https://nodejs.org) installed on your computer in order to build this app.

```bash
$ cd test-sorocabacom
$ yarn install
$ yarn dev
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
