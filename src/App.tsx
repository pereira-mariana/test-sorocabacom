import React from 'react';
import CharactersContainer from './components/CharactersContainer';
import Footer from './components/Footer';
import Form from './components/Form';

import Header from './components/Header';
import Main from './components/Main';

import GlobalStyle from './styles/global';

function App() {
  return (
    <>
      <GlobalStyle />
      <Header />
      <Main />
      <CharactersContainer />
      <Form />
      <Footer />
    </>
  );
}

export default App;
