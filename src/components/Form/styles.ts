import styled from 'styled-components';

export const Container = styled.div`
  position: relative;
  width: 100%;
  height: 782px;
  background: transparent linear-gradient(143deg, var(--blue) 0%, var(--primary-green) 100%);
  display: flex;
  justify-content: center;

  @media (max-width: 768px) {

  }
`;

export const FormContainer = styled.div`
  position: absolute;
  top: -25px;
  width: 1082px;
  height: 832px;
  background: var(--white);
  box-shadow: 0px 0px 6px var(--medium-gray);
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  align-items: center;

  h3 {
    width: 246px;
    height: 43px;
    color: var(--secondary-green);
    text-transform: uppercase;
    font-family: Montserrat;
    font-weight: 700;
    font-size: 35px;
    margin-top: 83px;
  }

  p {
    width: 746px;
    height: 77px;
    font-family: 'Open Sans';
    font-size: 20px;
    color: var(--background);
    margin-top: 41px;
  }

  form {
    display: flex;
    flex-direction: column;
    margin-top: 51px;

    div {
      display: flex;
      align-items: center;
      justify-content: center;

      input {
        width: 249px;
        height: 48px;
        border: 1px solid var(--background);
        padding: 12px 16px;

        &:not(:first-child) {
          margin-left: 30px;
        }
      }
    }

    textarea {
      margin-top: 40px;
      width: 528px;
      height: 197px;
      border: 1px solid var(--background);
      padding: 14px 15px;
    }

    button {
      width: 249px;
      height: 48px;
      background: var(--secondary-green);
      color: var(--white);
      font-size: 20px;
      font-family: 'Open Sans';
      text-transform: uppercase;
      margin-top: 50px;
    }
  }

  @media (max-width: 768px) {
    width: 668px;

    p {
      width: 548px;
      height: fit-content;
      font-size: 16px;
    }
  }

  @media (max-width: 768px) {
    width: 100%;
    padding: 10px;

    p {
      width: 95%;
      height: fit-content;
      font-size: 14px;
    }
  }

  @media (max-width: 400px) {
    width: 100%;
    padding: 10px;

    p {
      width: 95%;
      height: fit-content;
      font-size: 14px;
    }

    form {
      width: 100%;
      align-items: center;

      div {
        flex-direction: column;

        input {
          &:not(:first-child) {
            margin-left: 0;
            margin-top: 30px;
          }
        }
      }

      textarea {
        width: 100%;
      }
    }
  }
`;

