import * as Yup from 'yup';

const formSchema = Yup.object().shape({
  name: Yup
    .string()
    .required('O nome é obrigatório'),
  email: Yup
    .string()
    .email()
    .required('O email é obrigatório'),
  message: Yup
    .string()
    .required('A senha é obrigatória'),
});

export default formSchema;
