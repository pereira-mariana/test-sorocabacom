import React, { FormEvent, useState } from 'react';

import schema from './schema';

import { Container, FormContainer } from './styles';

const Form: React.FC = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');

  const handleSubmit = async (event: FormEvent) => {
    event.preventDefault();

    try {
      await schema.validate({ name, email, message });

      alert('Mensagem enviada!');
    } catch (error) {
      alert('Preencha todos os campos!');
    }
  };

  return (
    <Container>
      <FormContainer>
        <h3>Formulário</h3>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </p>
        <form onSubmit={handleSubmit}>
          <div>
            <input
              type="text"
              placeholder="Nome"
              value={name}
              onChange={event => setName(event.target.value)}
            />
            <input
              type="text"
              placeholder="Email"
              value={email}
              onChange={event => setEmail(event.target.value)}
            />
          </div>
          <textarea
            placeholder="Mensagem"
            value={message}
            onChange={event => setMessage(event.target.value)}
          />
          <button type="submit">Enviar</button>
        </form>
      </FormContainer>
    </Container>
  );
}

export default Form;
