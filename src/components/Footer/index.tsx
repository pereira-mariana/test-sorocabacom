import React from 'react';
import { MdKeyboardArrowUp } from 'react-icons/md';

import { Container } from './styles';

const Footer: React.FC = () => {
  return (
    <Container>
      <div>
        <MdKeyboardArrowUp />
      </div>
    </Container>
  );
}

export default Footer;
