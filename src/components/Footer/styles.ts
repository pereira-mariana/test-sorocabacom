import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 410px;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  div {
    width: 107px;
    height: 107px;
    background: var(--white);
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 100px;
    margin-right: 54px;

    svg {
      fill: var(--background);
      font-size: 100px;
    }
  }

  @media (max-width: 768px) {
    height: 285px;

    div {
      width: 78px;
      height: 78px;
    }
  }

  @media (max-width: 400px) {
    height: 185px;

    div {
      width: 48px;
      height: 48px;
      margin-bottom: 50px;
    }
  }
`;
