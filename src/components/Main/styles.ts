import styled from 'styled-components';

import background from '../../assets/Imagem 2.png';
import card from '../../assets/Imagem card.svg';

import feather1 from '../../assets/Sem Título-3.png';
import feather2 from '../../assets/Sem Título-3-1.png';
import feather3 from '../../assets/Sem Título-3-2.png';
import feather4 from '../../assets/Sem Título-3-3.png';

export const Container = styled.div`
  width: 100%;
  height: 971px;
  background: transparent url("${background}");
  display: flex;
  justify-content: center;

  @media (max-width: 400px) {
    height: 721px;
  }
`;

export const Card = styled.div`
  position: relative;
  width: 586px;
  height: 100%;
  background: transparent url("${card}") center center fixed;
  display: flex;
  flex-direction: column;
  align-items: center;

  span {
    margin-top: 12px;
    width: 230px;
    height: 20px;
    font: 15px 'Open Sans';
    letter-spacing: 0px;
    color: var(--white);
    align-self: flex-start;
    margin-left: 30px;
  }

  img {
    width: 526px;
    height: 754px;
    box-shadow: 0px 3px 6px #00000099;
    border-radius: 196px;
    margin-top: 22px;
    margin-bottom: 28px;
  }

  p {
    font-family: 'Open Sans';
    font-size: 20px;
    text-align: center;
    color: var(--off-white);
    letter-spacing: 0px;
    text-shadow: 0px 3px 6px var(--light-gray);
    width: 271px;
    height: 121px;
  }

  #feather1 {
    background: transparent url("${feather1}") 0% 0% no-repeat padding-box;
    position: absolute;
    width: 130px;
    height: 119px;
    top: 360px;
    left: -55px;
  }

  #feather2 {
    background: transparent url("${feather2}") 0% 0% no-repeat padding-box;
    position: absolute;
    top: 495px;
    left: 15px;
    width: 96px;
    height: 100px;
  }

  #feather3 {
    background: transparent url("${feather3}") 0% 0% no-repeat padding-box;
    position: absolute;
    top: 457px;
    right: -78px;
    width: 167px;
    height: 155px;
  }

  #feather4 {
    background: transparent url("${feather4}") 0% 0% no-repeat padding-box;
    position: absolute;
    top: 310px;
    right: -10px;
    width: 200px;
    height: 155px;
  }

  @media (max-width: 400px) {
    width: 100%;

    span {
      align-self: center;
    }

    img {
      width: 95%;
      height: 462px;
      border-radius: 100px;
    }

    #feather1,
    #feather2,
    #feather3,
    #feather4 {
      display: none;
    }
  }
`;
