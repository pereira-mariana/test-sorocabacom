import React from 'react';

import character from '../../assets/Imagem card-1.png';

import { Container, Card } from './styles';

const Main: React.FC = () => {
  return (
    <Container>
      <Card>
        <span>TRANSISTOR - RED THE SINGER</span>
        <img src={character} alt="RED THE SINGER"/>
        <p>"Olha, o que quer que você esteja pensando, me faça um favor, não solte."</p>
        <div id="feather1" />
        <div id="feather2" />
        <div id="feather3" />
        <div id="feather4" />
      </Card>
    </Container>
  );
}

export default Main;
