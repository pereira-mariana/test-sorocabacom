import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 109px;
  display: flex;
  align-items: center;
  justify-content: center;

  strong {
    text-transform: uppercase;
    font-family: Montserrat;
    font-size: 22.5px;
    color: var(--white);
  }

  @media (max-width: 400px) {


    strong {
      font-size: 18px;
    }
  }
`;
