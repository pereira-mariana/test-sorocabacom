import React from 'react';
import logo from '../../assets/Imagem 3.png';

import { Container } from './styles';

const Header: React.FC = () => {
  return (
    <Container>
      <img src={logo} alt="Super Giant Games"/>
      <strong>SuperGiantGames</strong>
    </Container>
  );
}

export default Header;
