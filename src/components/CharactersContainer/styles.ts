import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 1147px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  svg {
    fill: var(--white);
    font-size: 200px;

    @media (max-width: 1400px) {
      font-size: 100px;
    }

    @media (max-width: 768px) {
      display: none;
    }
  }

  @media (max-width: 768px) {
    height: fit-content;
  }
`;

export const CardContainer = styled.ul`
  width: 1366px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-top: 150px;

  @media (max-width: 1400px) {
    width: 1100px;
  }

  @media (max-width: 768px) {
    width: 100%;
    flex-direction: column;
    padding: 250px 0;
    justify-content: space-between;
  }
`;

export const Card = styled.li`
  position: relative;
  width: 387px;
  height: 613px;
  background: var(--white);
  border-radius: 31px;
  display: flex;
  justify-content: center;

  div {
    position: absolute;
    top: -103px;
    width: 364px;
    height: 440px;
    background: var(--background);
    border: 3px solid var(--white);
    border-radius: 86px;

    img {
      position: absolute;
      top: -86px;
      left: -20px;
      width: 385px;
      height: 533px;
    }
  }

  p {
    position: absolute;
    top: 379px;
    width: 326px;
    color: var(--background);
    font-family: 'Open Sans';
    font-size: 20px;
  }

  @media (max-width: 1400px) {
    width: 300px;
    height: 524px;
    border-radius: 23px;

    div {
      width: 283px;
      height: 368px;
      border-radius: 74px;

      img {
        top: -66px;
        left: -20px;
        width: 316px;
        height: 438px;
      }
    }

    p {
      top: 325px;
      width: 250px;
      color: var(--background);
      font-family: 'Open Sans';
      font-size: 15px;
    }
  }

  @media (max-width: 768px) {
    width: 387px;
    height: 613px;
    border-radius: 31px;

    &:not(:first-child) {
      margin-top: 200px;
    }

    div {
      width: 364px;
      height: 440px;
      border-radius: 86px;

      img {
        width: 287px;
        height: 443px;
        top: 0px;
        left: 14px;
      }
    }

    p {
      top: 400px;
      width: 250px;
      color: var(--background);
      font-family: 'Open Sans';
      font-size: 15px;
    }
  }

  @media (max-width: 400px) {
    width: 320px;
    height: 513px;
    border-radius: 31px;

    &:not(:first-child) {
      margin-top: 200px;
    }

    div {
      width: 305px;
      height: 368px;

      img {
        width: 265px;
        height: 412px;
        top: -41px;
      }
    }

    p {
      top: 350px;
      width: 250px;
      color: var(--background);
      font-family: 'Open Sans';
      font-size: 15px;
    }
  }
`;
