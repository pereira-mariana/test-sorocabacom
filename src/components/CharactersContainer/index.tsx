import React from 'react';
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from 'react-icons/md';

import Sybil from '../../assets/Sybil_2.png';
import Grant from '../../assets/Grant.png';
import Red from '../../assets/Red.png';

import { Container, CardContainer, Card } from './styles';

const CharactersContainer: React.FC = () => {
  return (
    <Container>
      <MdKeyboardArrowLeft />
      <CardContainer>
        <Card>
          <div>
            <img src={Grant} alt="Grant"/>
          </div>
          <p>
            A Camerata foi apenas os dois no início, e suas fileiras nunca foram destinadas a exceder um número a ser contado em uma mão.
          </p>
        </Card>
        <Card>
          <div>
            <img src={Red} alt="Red"/>
          </div>
          <p>
          Red, uma jovem cantora, entrou em posse do Transistor. Sendo a poderosa espada falante. O grupo Possessores quer tanto ela quanto o Transistor e está perseguindo implacavelmente a sua procura.
          </p>
        </Card>
        <Card>
          <div>
            <img src={Sybil} alt="Sybil"/>
          </div>
          <p>Sybil é descrita pelo Transistor como sendo os "olhos e ouvidos" da Camerata.</p>
        </Card>
      </CardContainer>
      <MdKeyboardArrowRight />
    </Container>
  );
}

export default CharactersContainer;
