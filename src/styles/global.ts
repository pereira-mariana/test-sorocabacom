import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    background: var(--background) 0% 0% no-repeat padding-box;
  }

  html, body, #root {
    height: 100%;
  }

  *, button, input {
    border: 0;
  }

  :root {
    --black: #000000;
    --white: #FFFFFF;
    --alpha-white: #FFFFFF63;
    --off-white: #F0F0F2;
    --light-gray: #00000029;
    --medium-gray: #0000004D;
    --dark-gray: #707070;
    --background: #363636;
    --blue: #7DEDE2;
    --primary-green: #58B790;
    --secondary-green: #63C7A9;
    --tertiary-green: #2AC496;
  }
`;
